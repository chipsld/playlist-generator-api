Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  resources :tracks
  resources :playlists
  post 'playlists/generate', to: 'playlists#generate'
  resources :users, param: :_email
  post '/login', to: 'auth#login'
  get '/genres', to: 'genres#index'
end
