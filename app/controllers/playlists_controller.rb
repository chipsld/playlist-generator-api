class PlaylistsController < ApplicationController
  before_action :authorize_request
  def index
    @playlists = Playlist.where(user_id: @current_user.id).includes(:tracks)
    render json: @playlists.to_json(include: [:tracks])
  end

  def create
    @playlist = Playlist.new(name: params[:name], user_id: @current_user.id)
    @playlist.save

    params[:tracks].each do |track|
      @track = Track.new(title: track['title'], artist: track['artist'], duration: track['duration'], playlist: @playlist)
      @track.save
    end

    if @playlist.valid?
      render json: { data: 'Playlist saved successfully'}, status: 200
    else
      render json: { error: 'error' }, status: 401
    end
  end

  def generate

    genres = params[:genres]
    RSpotify.authenticate('01e6b11039fc49af89f4d0d4a41a6360', '55317b9ab2d14b179c9d3fb5b9af40be')
    playlist_songs = RSpotify::Recommendations.generate(limit: 20, seed_genres: genres, target_energy: 1.0)


    @tracks = []
    playlist_songs.tracks.each do |track|
      @track = Track.new(title: track.name, artist: track.artists[0].name, duration: track.duration_ms/ 1000 / 60, playlist: @playlist)
      @tracks.push(@track)
    end

    if playlist_songs
      render json: { tracks: @tracks }, status: 200
    else
      render json: { error: 'error' }, status: 401
    end
  end


  def delete
    @playlist = Playlist.find(params[:id])
    if @playlist
      @playlist.destroy
      render json: { data: 'Playlist deleted successfully' }, status: 200
    else
      render json: { error: 'Playlist not found' }, status: 404
    end

  end


  private

  def validate_params
    params.permit(:name)
  end

end
