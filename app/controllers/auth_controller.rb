class AuthController < ApplicationController
  before_action :authorize_request, except: :login
  def login
    @user = User.find_by(email: params[:email])
    if @user&.authenticate(params[:password])
      token = encode_token({ user_id: @user.id})
      render json: { user: @user, token: token}, status: 200
    else
      render json: { error: 'unauthorized' }, status: :unauthorized
    end
  end

end

