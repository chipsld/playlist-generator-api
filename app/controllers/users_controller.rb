class UsersController < ApplicationController
  def create
    @user = User.create(validate_params)
    if @user.valid?
      token = encode_token({ user_id: @user.id})
      render json: { user: @user, token: token}, status: 200
    else
      render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
    end
  end

  private
  def validate_params
    params.permit(:email,:password,:firstname,:lastname)
  end
end