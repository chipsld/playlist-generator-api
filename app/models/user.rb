class User < ApplicationRecord
  has_secure_password
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }, uniqueness: true
  validates :password, confirmation: true

  def as_json(options = {})
    options[:except] ||= [:password_digest]
    super(options)
  end
end
