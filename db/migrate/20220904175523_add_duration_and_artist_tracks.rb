class AddDurationAndArtistTracks < ActiveRecord::Migration[7.0]
  def change
    add_column :tracks, :artist, :string
    add_column :tracks, :duration, :float
  end
end
